using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEditor;
using UnityEngine;

public class TargetingSystem : MonoBehaviour
{
    [SerializeField] private TowerStatSO towerStatSO;

    [SerializeField] private Transform firePointTransform;
    [SerializeField] private Transform towerHeadTransform;

    private Enemy targetEnemy;

    private float timeBetweenShot;

    private void Update() {

        Vector3 targetPositionOffset = new Vector3(transform.position.x, 0, transform.position.z);
        Collider[] colliderArray = Physics.OverlapSphere(targetPositionOffset, towerStatSO.range);

        if (HasTarget()) {
            Vector3 targetEnemyPosition = new Vector3(targetEnemy.transform.position.x, 0, targetEnemy.transform.position.z);
            Vector3 towerHeadTransformPosition = new Vector3(towerHeadTransform.position.x, 0, towerHeadTransform.position.z);
            Vector3 lookDir = (targetEnemyPosition - towerHeadTransformPosition).normalized;
            float rotateSpeed = 10f;
            Quaternion lookRotation = Quaternion.LookRotation(lookDir);
            towerHeadTransform.rotation = Quaternion.Slerp(towerHeadTransform.rotation, lookRotation, rotateSpeed * Time.deltaTime);

            timeBetweenShot -= Time.deltaTime;
            if(timeBetweenShot < 0) {
                Fire();
            }
        }

        if(colliderArray != null) {
            foreach(Collider collider in colliderArray) {
                if(collider.TryGetComponent<Enemy>(out Enemy enemy)) {
                    if(targetEnemy != null) {
                        float currentTargetDistance = Vector3.Distance(targetEnemy.transform.position, transform.position);
                        float newTargetDistance = Vector3.Distance(enemy.transform.position, transform.position);

                        if(newTargetDistance < currentTargetDistance) {
                            targetEnemy = enemy;
                        }
                    }
                    else {
                        targetEnemy = enemy;
                    }
                    break;
                }
                else {
                    continue;
                }
            }
        }
    }

    private void Fire() {
        //Shoot Projectile
        Projectile.SpawnProjectile(targetEnemy, towerStatSO, firePointTransform);
        //Reset Timer
        timeBetweenShot = towerStatSO.attackSpeed;

    }

    private bool HasTarget() {
        return targetEnemy != null;
    }

    private void OnDrawGizmos() {
        //Vector3 targetPositionOffset = new Vector3(transform.position.x, 0, transform.position.z);
        //Gizmos.DrawSphere(targetPositionOffset, towerStatSO.range);
    }

}
