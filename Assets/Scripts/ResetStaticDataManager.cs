using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetStaticDataManager : MonoBehaviour
{
    private void Awake() {
        SpawnPoint.ResetStaticData();
        Enemy.ResetStaticData();
        MainBase.ResetStaticData();
        ContainerNode.ResetStaticData();
    }
}
