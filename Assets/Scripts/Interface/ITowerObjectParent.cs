using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITowerObjectParent
{
    public Transform GetPlacementPoint();
    public void SetTowerObject(TowerObject towerObject);
    public TowerObject GetTowerObject();
    public bool HasTowerObject();
    public void ClearTowerObject();

}
