using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProjectile
{
    public void SetStartingPosition(Vector3 startingPos);
    public Vector3 InitArcPoint(Vector3 startPos, Vector3 endPos);
    public Vector3 GetArcPoint();
    public void SetArcPoint(Vector3 arcPoint);
    public ProjectileTypeSO GetProjectileTypeSO();
    public Enemy GetTargetEnemy();
    public void SetTargetEnemy(Enemy targetEnemy);
    public Vector3 GetFirePoint();
    public void SetFirePoint(Vector3 firePoint);
    public float GetDamage();
    public void SetDamage(float damage);
    public float GetLerpAmount();
    public void SetLerpAmount(float amount);
    public bool HasEnemyTarget();
    public bool HasProjectileLifeTimeExpire();

    public void DestroySelf();

}
