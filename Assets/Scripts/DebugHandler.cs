using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DebugHandler
{
    public static void DebugPlayerCapsule(Transform playerTransform, Transform debugTransform, Vector3 moveDir, float moveDistance) {
        debugTransform.position = Vector3.Lerp(debugTransform.position, playerTransform.position + moveDir, moveDistance);
    }
}
