using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBase : MonoBehaviour
{
    public static event EventHandler OnMainBaseDestroy;
    public static void ResetStaticData() {
        OnMainBaseDestroy = null;
    }

    public static MainBase instance;

    private HealthSystem healthSystem;
    private BoxCollider boxCollider;
    private Vector3 mainBaseTargetOffset;

    private void Awake() {
        instance = this;
        healthSystem = GetComponent<HealthSystem>();
        boxCollider = GetComponent<BoxCollider>();
    }

    private void Start() {
        healthSystem.OnDie += HealthSystem_OnDie;
    }

    private void HealthSystem_OnDie(object sender, EventArgs e) {
        OnMainBaseDestroy?.Invoke(this, EventArgs.Empty);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.TryGetComponent<Enemy>(out Enemy enemy)) {
            float damage = enemy.GetEnemyObjectSO().damage;
            healthSystem.Damage(damage);
            enemy.DestroySelf();
        }
    }

public Vector3 GetMainBaseTargetOffset() {
        float offsetX = UnityEngine.Random.Range(boxCollider.size.x / 2, -boxCollider.size.x / 2);
        float offsetZ = UnityEngine.Random.Range(boxCollider.size.z / 2, -boxCollider.size.z / 2);

        mainBaseTargetOffset = new Vector3(offsetX, 0, offsetZ);

        return mainBaseTargetOffset;
    }



}
