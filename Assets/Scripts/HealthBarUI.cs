using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUI : MonoBehaviour
{
    [SerializeField] private HealthSystem healthSystem;
    [SerializeField] private Image barImage;
    [SerializeField] private TextMeshProUGUI text;

    private void Start() {
        healthSystem.OnHealthChanged += HealthSystem_OnHealthChanged;

        barImage.fillAmount = 1;
        text.text = (barImage.fillAmount * 100f).ToString() + "%";



    }

    private void HealthSystem_OnHealthChanged(object sender, HealthSystem.OnHealthChangedEventArgs e) { 
        barImage.fillAmount = e.healthNormalized;
        text.text = (e.healthNormalized * 100).ToString() + "%";
        if(e.healthNormalized == 0 || e.healthNormalized == 1) {
            Hide();
        }
        else {
            Show();
        }
    }

    public void Show() {
        gameObject.SetActive(true);
    }

    public void Hide() {
        gameObject.SetActive(false);
    }
}
