using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerNodeVisual : MonoBehaviour
{
    private const string INTERACT = "Interact";

    [SerializeField] private ContainerNode containerNode;

    private Animator animator;

    private void Awake() {
        animator = GetComponent<Animator>();
    }

    private void Start() {
        containerNode.OnPlayerGrabObject += ContainerNode_OnPlayerGrabObject;
    }

    private void ContainerNode_OnPlayerGrabObject(object sender, System.EventArgs e) {
        animator.SetTrigger(INTERACT);
    }
}
