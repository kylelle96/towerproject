using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MergerNodeVisual : MonoBehaviour
{
    private const string MERGE = "Merge";

    [SerializeField] private MergerNode mergerNode;

    private Animator animator;

    private void Awake() {
        animator = GetComponent<Animator>();
    }

    private void Start() {
        mergerNode.OnMerge += MergerNode_OnMerge;
    }

    private void MergerNode_OnMerge(object sender, System.EventArgs e) {
        animator.SetTrigger(MERGE);
    }
}
