using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashNode : BaseNode
{
    public override void Interact(Player player) {
        if (player.HasTowerObject()) {
            player.GetTowerObject().DestroySelf();
        }
    }
}
