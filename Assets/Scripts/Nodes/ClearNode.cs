using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ClearNode : BaseNode
{
    [SerializeField] private TowerObjectSO towerObjectSO;

    public override void Interact(Player player) {
        if (!HasTowerObject()) {
            //There is no Tower Object here
            if (player.HasTowerObject()) {
                //Player is Carrying something
                player.GetTowerObject().SetTowerObjectParent(this);
            }
            else {
                //Player not carrying anything
            }
        }
        else {
            //There is Tower Object here
            if (player.HasTowerObject()) {
                //Player is Carrying something
            }
            else {
                //Player not carrying anything
                GetTowerObject().SetTowerObjectParent(player);
            }
        }
    }
}
