using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseNode : MonoBehaviour, ITowerObjectParent
{
    [SerializeField] private Transform placementPoint;

    private TowerObject towerObject;

    public virtual void Interact(Player player) {
        Debug.LogError("BaseNode.Interact();");
    }

    public virtual void InteractAlternate(Player player) {
        Debug.LogError("BaseNode.InteractAlternate();");
    }

    public Transform GetPlacementPoint() {
        return placementPoint;
    }
    public TowerObject GetTowerObject() {
        return towerObject;
    }
    public void SetTowerObject(TowerObject towerObject) {
        this.towerObject = towerObject;
    }
    public void ClearTowerObject() {
        towerObject = null;
    }
    public bool HasTowerObject() {
        return towerObject != null;
    }



}
