using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MergerNode : BaseNode
{
    public event EventHandler<OnProgressChangedEventArgs> OnProgressChanged;
    public class OnProgressChangedEventArgs : EventArgs {
        public float progressNormalized;
    }
    public event EventHandler OnMerge;

    [SerializeField] private MergerNode mergerLinkNode;
    [SerializeField] private MergedRecipeSO[] mergedRecipeSOArray;

    private int mergingProgress;


    public override void Interact(Player player) {
        if (!HasTowerObject()) {
            //There is no Tower Object here
            if (player.HasTowerObject()) {
                //Player is Carrying something
                player.GetTowerObject().SetTowerObjectParent(this);
                mergingProgress = 0;
                mergerLinkNode.mergingProgress = 0;

                mergerLinkNode.OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs {
                    progressNormalized = mergerLinkNode.mergingProgress
                }) ;

                if (mergerLinkNode.HasTowerObject()) {
                    OnProgressChangedWithRecipeInputs();
                }
            }
            else {
                //Player not carrying anything
            }
        }
        else {
            //There is Tower Object here
            if (player.HasTowerObject()) {
                //Player is Carrying something
                if (!mergerLinkNode.HasTowerObject()) {
                    //Link Node has no tower object yet
                    GetTowerObject().SetTowerObjectParent(mergerLinkNode);
                    player.GetTowerObject().SetTowerObjectParent(this);
                    mergingProgress = 0;
                    mergerLinkNode.mergingProgress = 0;

                    mergerLinkNode.OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs {
                        progressNormalized = mergerLinkNode.mergingProgress
                    });

                    OnProgressChangedWithRecipeInputs();
                }
            }
            else {
                //Player not carrying anything
                GetTowerObject().SetTowerObjectParent(player);
                mergingProgress = 0;
                mergerLinkNode.mergingProgress = 0;
                OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs {
                    progressNormalized = mergingProgress
                });
                mergerLinkNode.OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs {
                    progressNormalized = mergerLinkNode.mergingProgress
                });
            }
        }
    }



    public override void InteractAlternate(Player player) {
        if (HasTowerObject() && mergerLinkNode.HasTowerObject()) {
            //Has Tower Object on Both Merge Nodes
            TowerObjectSO[] inputTowerObjectSOArray = new TowerObjectSO[]{GetTowerObject().GetTowerObjectSO(), mergerLinkNode.GetTowerObject().GetTowerObjectSO()};

            if (HasRecipeWithInput(inputTowerObjectSOArray)) {
                //It is Valid to Merge this Two Tower Object
                mergingProgress++;
                mergerLinkNode.mergingProgress++;

                OnMerge?.Invoke(this, EventArgs.Empty);
                mergerLinkNode.OnMerge?.Invoke(this, EventArgs.Empty);

                MergedRecipeSO mergedRecipeSO = GetMergingRecipeSOWithInput(inputTowerObjectSOArray);
                OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs {
                    progressNormalized = (float)mergingProgress / mergedRecipeSO.mergingProgressMax
                });
                mergerLinkNode.OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs {
                    progressNormalized = 0
                });


                if (mergingProgress >= mergedRecipeSO.mergingProgressMax) {
                    TowerObjectSO outputTowerObject = GetOutputForInput(inputTowerObjectSOArray);
                    GetTowerObject().DestroySelf();
                    mergerLinkNode.GetTowerObject().DestroySelf();

                    TowerObject.SpawnTowerObject(outputTowerObject, this);
                }
            }
        }
    }

    private void OnProgressChangedWithRecipeInputs() {
        TowerObjectSO[] inputTowerObjectSOArray = new TowerObjectSO[] { GetTowerObject().GetTowerObjectSO(), mergerLinkNode.GetTowerObject().GetTowerObjectSO() };
        if (HasRecipeWithInput(inputTowerObjectSOArray)) {
            MergedRecipeSO mergedRecipeSO = GetMergingRecipeSOWithInput(inputTowerObjectSOArray);
            OnProgressChanged?.Invoke(this, new OnProgressChangedEventArgs {
                progressNormalized = (float)mergingProgress / mergedRecipeSO.mergingProgressMax
            });
        }
    }

    private bool HasRecipeWithInput(TowerObjectSO[] inputTowerObjectSOArray) {
        MergedRecipeSO mergedRecipeSO = GetMergingRecipeSOWithInput(inputTowerObjectSOArray);
        return mergedRecipeSO != null;
    }

    private TowerObjectSO GetOutputForInput(TowerObjectSO[] inputTowerObjectSOArray) {
        MergedRecipeSO mergedRecipeSO = GetMergingRecipeSOWithInput(inputTowerObjectSOArray);
        if(mergedRecipeSO != null) {
            return mergedRecipeSO.output;
        }
        else {
            return null;
        }
       
    }

    private MergedRecipeSO GetMergingRecipeSOWithInput(TowerObjectSO[] inputTowerObjectSOArray) {
        foreach (MergedRecipeSO mergedRecipeSO in mergedRecipeSOArray) {
            if (mergedRecipeSO.inputList.Count == inputTowerObjectSOArray.Length) {
                bool isValidRecipe = true;
                List<TowerObjectSO> mergedObject = new List<TowerObjectSO>();
                foreach (TowerObjectSO inputTowerObjectSO in inputTowerObjectSOArray) {
                    if (mergedRecipeSO.inputList.Contains(inputTowerObjectSO) && !mergedObject.Contains(inputTowerObjectSO)) {
                        isValidRecipe = true;
                        mergedObject.Add(inputTowerObjectSO);
                    }
                    else {
                        isValidRecipe = false;
                        break;
                    }
                }
                if (!isValidRecipe) {
                    continue;
                }
                return mergedRecipeSO;
            }
        }
        return null;
    }

}
