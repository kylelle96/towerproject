using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerNode : BaseNode
{
    public static event EventHandler<OnPurchaseEventArgs> OnPurchase;
    public static void ResetStaticData() {
        OnPurchase = null;
    }
    public event EventHandler OnPlayerGrabObject;

    public class OnPurchaseEventArgs : EventArgs {
        public int cost;
    }

    [SerializeField] private TowerObjectSO towerObjectSO;
    [SerializeField] private int cost;


    public override void Interact(Player player) {
        if (!player.HasTowerObject() && TowerGameManager.Instance.GetCurrency() >= cost) {
            //Player not carrying anything
            TowerObject.SpawnTowerObject(towerObjectSO, player);
            
            OnPlayerGrabObject?.Invoke(this, EventArgs.Empty);
            OnPurchase?.Invoke(this, new OnPurchaseEventArgs {
                cost = cost
            });
        }
    }

    public int GetCost() {
        return cost;
    }


}
