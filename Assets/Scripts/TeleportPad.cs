using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPad : MonoBehaviour
{
    [SerializeField] TeleportPad teleportLinkPad;
    [SerializeField] Transform teleportPoint;


    private float teleportTimer = 0f;
    private float teleportTimerMax = 3f;
    private bool isTeleportEnabled = false;
    private Player player;

    private BoxCollider boxCollider;

    private void Awake() {
        boxCollider = GetComponent<BoxCollider>();
    }
    private void Update() {
        Collider[] colliderArray = Physics.OverlapBox(transform.position, boxCollider.size/2);

        foreach(Collider collider in colliderArray) {
            if(collider.TryGetComponent<Player>(out Player player)){
                isTeleportEnabled = true;
                this.player = player;
                break;
            }
            else {
                isTeleportEnabled = false;
                teleportTimer = 0f;
                this.player = null;
                continue;
            }

        }

        if (isTeleportEnabled) {
            teleportTimer += Time.deltaTime;
            if(teleportTimer >= teleportTimerMax) {
                Debug.Log("Teleport");
                player.SetTeleportLocation(teleportLinkPad.teleportPoint.position);
                teleportTimer = 0f;
            }
        }

    }

}
