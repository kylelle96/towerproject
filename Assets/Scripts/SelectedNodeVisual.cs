using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedNodeVisual : MonoBehaviour
{
    [SerializeField] private BaseNode baseNode;
    [SerializeField] private GameObject[] visualGameObjectArray;


    private void Start() {
        Player.Instance.OnSelectedNodeChanged += Player_OnSelectedNodeChanged;

    }

    private void Player_OnSelectedNodeChanged(object sender, Player.OnSelectedNodeChangedEventArgs e) {
        if(e.selectedNode == baseNode) {
            Show();
        }
        else {
            Hide();
        }
    }

    private void Show() {
        foreach(GameObject visualGameObject in visualGameObjectArray) {
           visualGameObject.SetActive(true);
        }
    }

    private void Hide() {
        foreach (GameObject visualGameObject in visualGameObjectArray) {
            visualGameObject.SetActive(false);
        }
    }
}
