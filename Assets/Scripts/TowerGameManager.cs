using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerGameManager : MonoBehaviour
{
    public event EventHandler<OnWaveInitializedEventArgs> OnWaveInitialized;
    public event EventHandler<OnWaveCountChangedEventArgs> OnWaveCountChanged;
    public event EventHandler<OnCurrencyChangedEventArgs> OnCurrencyChanged;
    public event EventHandler<OnEnemyCountChangedEventArgs> OnEnemyCountChanged;
    public event EventHandler OnStateChanged;
    public event EventHandler OnGamePaused;
    public event EventHandler OnGameUnpaused;


    public class OnWaveInitializedEventArgs : EventArgs {
        public int enemyCount;
    }

    public class OnWaveCountChangedEventArgs : EventArgs { 
        public int waveCount; 
    }

    public class OnCurrencyChangedEventArgs : EventArgs {
        public int currency;
        public int amount;
        public Color color;
    }

    public class OnEnemyCountChangedEventArgs : EventArgs {
        public int enemyCountMax;
    }

    public static TowerGameManager Instance { get; private set; }

    //Create State the game can be in
    public enum State {
        WaitingToStart,
        CountdownToStart,
        WaveInitializing,
        GamePlaying,
        GameOver,
    }

    private State state;
    private float waitingToStartTimer = 1f;
    private float countdownToStartTimer = 3f;

    private int waveCount = 0;
    private int enemyCount = 0;
    private float nextWaveTimer = 15f;

    private int enemyCountMax;
    private int currency;

    private bool isGamePaused;

    private void Awake() {
        Instance = this;
        state = State.WaitingToStart;
    }

    private void Start() {
        GameInput.Instance.OnPauseAction += GameInput_OnPauseAction;
        SpawnPoint.OnSetSpawnCount += SpawnPoint_OnSetSpawnCount;
        Enemy.OnEnemyDestroy += Enemy_OnEnemyDestroy;
        MainBase.OnMainBaseDestroy += MainBase_OnMainBaseDestroy;
        ContainerNode.OnPurchase += ContainerNode_OnPurchase;
        OnWaveCountChanged?.Invoke(this, new OnWaveCountChangedEventArgs {
            waveCount = waveCount,
        });

    }

    private void GameInput_OnPauseAction(object sender, EventArgs e) {
        TogglePauseGame();
    }

    private void OnDestroy() {
        GameInput.Instance.OnPauseAction -= GameInput_OnPauseAction;
        SpawnPoint.OnSetSpawnCount -= SpawnPoint_OnSetSpawnCount;
        Enemy.OnEnemyDestroy -= Enemy_OnEnemyDestroy;
        MainBase.OnMainBaseDestroy -= MainBase_OnMainBaseDestroy;
        ContainerNode.OnPurchase -= ContainerNode_OnPurchase;
    }

    private void ContainerNode_OnPurchase(object sender, ContainerNode.OnPurchaseEventArgs e) {
        DecreaseCurrency(e.cost);
    }

    private void MainBase_OnMainBaseDestroy(object sender, EventArgs e) {
        SetCurrentState(State.GameOver);
        OnStateChanged?.Invoke(this, EventArgs.Empty);
    }

    private void Enemy_OnEnemyDestroy(object sender, EventArgs e) {
        DecreaseEnemyCountMax();
        OnEnemyCountChanged?.Invoke(this, new OnEnemyCountChangedEventArgs {
            enemyCountMax = enemyCountMax
        });
    }

    private void SpawnPoint_OnSetSpawnCount(object sender, SpawnPoint.OnSetSpawnCountEventArgs e) {
        SetEnemyCountMax(e.spawnCount);
        OnEnemyCountChanged?.Invoke(this, new OnEnemyCountChangedEventArgs {
            enemyCountMax = enemyCountMax
        });
    }

    private void Update() {
        switch (state) {
            case State.WaitingToStart:
                waitingToStartTimer -= Time.deltaTime;
                if (waitingToStartTimer < 0f) {
                    state = State.CountdownToStart;
                    int startingCurrency = 1000;
                    IncreaseCurrency(startingCurrency);
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case State.CountdownToStart:
                countdownToStartTimer -= Time.deltaTime;
                if(countdownToStartTimer < 0f) {
                    state = State.WaveInitializing;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }
                break;
            case State.WaveInitializing:
                nextWaveTimer -= Time.deltaTime;
                if (nextWaveTimer < 0f) {
                    state = State.GamePlaying;
                    waveCount++;
                    enemyCount = 2 * waveCount;
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                    OnWaveInitialized?.Invoke(this, new OnWaveInitializedEventArgs {
                        enemyCount = enemyCount,
                    });
                    OnWaveCountChanged?.Invoke(this, new OnWaveCountChangedEventArgs {
                        waveCount = waveCount,
                    });
                }

                break;
            case State.GamePlaying:
                if(enemyCountMax <= 0) {
                    state = State.WaveInitializing;
                    ResetWaveInitializeTimer(15f);
                    OnStateChanged?.Invoke(this, EventArgs.Empty);
                }

                break;
            case State.GameOver:
                Debug.Log(state);
                break;
        }
    }

    public State GetCurrentState() {
        return state;
    }

    public int GetCurrency() {
        return currency;
    }

    public bool IsGamePlaying() {
        return state == State.WaveInitializing || state == State.GamePlaying;
    }

    public bool IsCountdownToStartActive() {
        return state == State.CountdownToStart;
    }

    public bool IsWaveInitializing() {
        return state == State.WaveInitializing;
    }

    public bool IsGameOver() {
        return state == State.GameOver;
    }

    public float GetCountdownToStartTimer() {
        return countdownToStartTimer;
    }

    public float GetWaveInitializeTimer() {
        return nextWaveTimer;
    }

    public int GetCurrentWave() {
        return waveCount;
    }

    private void SetCurrentState(State state) {
        this.state = state;
    }

    private void DecreaseCurrency(int amount) {
        currency -= amount;
        OnCurrencyChanged?.Invoke(this, new OnCurrencyChangedEventArgs {
            currency = currency,
            amount = amount,
            color = Color.red
        });

    }

    private void IncreaseCurrency(int amount) {
        currency += amount;
        OnCurrencyChanged?.Invoke(this, new OnCurrencyChangedEventArgs {
            currency = currency,
            amount = amount,
            color = Color.green
        });
    }

    private void SetEnemyCountMax(int spawnCount) {
        enemyCountMax += spawnCount;
    }

    private void DecreaseEnemyCountMax() {
        enemyCountMax--;
        if(enemyCountMax < 0) {
            enemyCountMax = 0;
        }
    }

    private void ResetWaveInitializeTimer(float timer) {
        nextWaveTimer = timer;
    }

    public void TogglePauseGame() {
        isGamePaused = !isGamePaused;
        if (isGamePaused) {
            Time.timeScale = 0f;
            OnGamePaused?.Invoke(this, EventArgs.Empty);
        }
        else {
            Time.timeScale = 1f;
            OnGameUnpaused?.Invoke(this, EventArgs.Empty);
        }
    }
}
