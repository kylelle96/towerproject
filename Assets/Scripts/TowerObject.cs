using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerObject : MonoBehaviour
{
    [SerializeField] private TowerObjectSO towerObjectSO;

    private ITowerObjectParent towerObjectParent;

    public TowerObjectSO GetTowerObjectSO() {
        return towerObjectSO;
    }
    public ITowerObjectParent GetTowerObjectParent() {
        return towerObjectParent;
    }

    public void SetTowerObjectParent(ITowerObjectParent towerObjectParent) {
        if(this.towerObjectParent != null) {
            this.towerObjectParent.ClearTowerObject();
        }
        this.towerObjectParent = towerObjectParent;

        if (towerObjectParent.HasTowerObject()) {
            Debug.LogError("Already has a Tower Object");
        }

        towerObjectParent.SetTowerObject(this);

        transform.parent = towerObjectParent.GetPlacementPoint();
        //transform.localPosition = Vector3.zero;
        //transform.localRotation = Quaternion.identity;

    }
    public void SetLocalPosition(Vector3 positionToSet) {
        transform.localPosition = positionToSet;
    }

    public void SetLocalRotation(Quaternion rotationToSet) {
        transform.localRotation = rotationToSet;
    }

    private void Update() {
        if(towerObjectParent != null) {
            float moveSpeed = 10f;
            float rotationSpeed = 10f;
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, moveSpeed * Time.deltaTime);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.identity, rotationSpeed * Time.deltaTime);
        }
    }

    public void DestroySelf() {
        towerObjectParent.ClearTowerObject();
        Destroy(gameObject);
    }

    public static TowerObject SpawnTowerObject(TowerObjectSO towerObjectSO, ITowerObjectParent towerObjectParent) {
        if (towerObjectSO == null) { return null; }
        Transform towerObjectTransform = Instantiate(towerObjectSO.prefab);
        TowerObject towerObject = towerObjectTransform.GetComponent<TowerObject>();
        towerObject.SetTowerObjectParent(towerObjectParent);
        towerObject.SetLocalPosition(Vector3.zero);
        towerObject.SetLocalRotation(Quaternion.identity);

        return towerObject;
    }

}
