using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour,IProjectile
{
    [SerializeField] private ProjectileTypeSO projectileTypeSO;

    private Enemy targetEnemy;
    private Vector3 firePoint;
    private Vector3 arcPoint;
    private float damage;
    private float lerpAmount;

    protected float lifeTime;

    public virtual void ProjectilePath(Vector3 target) {
        Debug.LogError("BaseProjectile.ProjectilePath");
    }

    public virtual void ProjectilePathWithArcPoint(Vector3 target, Vector3 arcPoint) {
        Debug.LogError("BaseProjectile.ProjectilePathWithArcPoint");
    }


    public static BaseProjectile SpawnProjectile(Enemy targetEnemy, TowerStatSO towerStatSO, Transform firePoint) {
        Transform projectileTransform = Instantiate(towerStatSO.projectilePrefab);
        BaseProjectile projectile = projectileTransform.GetComponent<BaseProjectile>();
        projectile.SetStartingPosition(firePoint.position);
        projectile.SetTargetEnemy(targetEnemy);
        projectile.SetDamage(towerStatSO.damage);
        projectile.SetFirePoint(firePoint.position);

        Vector3 arcPoint = projectile.InitArcPoint(projectile.firePoint, projectile.targetEnemy.transform.position);
        projectile.SetArcPoint(arcPoint);

        return projectile;

    }

    protected void OnTriggerEnter(Collider other) {
        if (other.TryGetComponent<Enemy>(out Enemy enemy)) {
            HealthSystem enemyHealthSystem = enemy.GetComponent<HealthSystem>();
            enemyHealthSystem.Damage(GetDamage());
            DestroySelf();
        }
    }

    public Vector3 InitArcPoint(Vector3 startPos, Vector3 endPos) {
        Vector3 midPoint = (startPos + endPos) / 2;
        Vector3 arcHeight = new Vector3(0, 10f, 0);
        return midPoint + arcHeight;
    }

    public Vector3 GetArcPoint() {
        return arcPoint;
    }

    public ProjectileTypeSO GetProjectileTypeSO() {
        return projectileTypeSO;
    }

    public Enemy GetTargetEnemy() {
        return targetEnemy;
    }

    public Vector3 GetFirePoint() {
        return firePoint;
    }

    public float GetDamage() {
        return damage;
    }

    public float GetLerpAmount() {
        return lerpAmount;
    }

    public void SetStartingPosition(Vector3 startingPos) {
        transform.position = startingPos;
    }

    public void SetTargetEnemy(Enemy targetEnemy) {
        this.targetEnemy = targetEnemy;
    }

    public void SetFirePoint(Vector3 firePoint) {
        this.firePoint = firePoint;
    }

    public void SetDamage(float damage) {
        this.damage = damage;
    }

    public void SetLerpAmount(float amount) {
        this.lerpAmount = amount;
    }

    public void SetArcPoint(Vector3 arcPoint) {
        this.arcPoint = arcPoint;
    }

    public bool HasEnemyTarget() {
        return (this.targetEnemy != null);
    }

    public bool HasProjectileLifeTimeExpire() {
        return lifeTime >= GetProjectileTypeSO().projectileLifeTime;
    }

    public void DestroySelf() {
        Destroy(gameObject);
    }
}
