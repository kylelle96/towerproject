using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunProjectile : BaseProjectile
{
    public override void ProjectilePath(Vector3 target) {
        Vector3 offset = new Vector3(0, 1, 0);
        //Vector3 moveDir = ((target + offset)  - transform.position).normalized;

        //transform.position += moveDir * projectileTypeSO.projectileSpeed * Time.deltaTime;
        SetLerpAmount((GetLerpAmount() + (GetProjectileTypeSO().projectileSpeed * Time.deltaTime)));
        Vector3 firePointToTarget = Vector3.Lerp(GetFirePoint(), target + offset, GetLerpAmount());

        transform.position = firePointToTarget;
    }

    public override void ProjectilePathWithArcPoint(Vector3 target, Vector3 arcPoint) {
        Vector3 offset = new Vector3(0, 1, 0);
        SetLerpAmount((GetLerpAmount() + (GetProjectileTypeSO().projectileSpeed * Time.deltaTime)));

        Vector3 firePointToArcPoint = Vector3.Lerp(GetFirePoint(), arcPoint, GetLerpAmount());
        Vector3 arcPointToTarget = Vector3.Lerp(arcPoint, target, GetLerpAmount());
        Vector3 firePointToArcPoint_ArcPointToTarget = Vector3.Lerp(firePointToArcPoint, arcPointToTarget, GetLerpAmount());

        transform.position = firePointToArcPoint_ArcPointToTarget;

    }


    private void Update() {

        if (!HasEnemyTarget()) {
            DestroySelf();
            return;
        }

        ProjectilePath(GetTargetEnemy().transform.position);  // <- Linear Projectile Travel
        //ProjectilePathWithArcPoint(GetTargetEnemy().transform.position, GetArcPoint()); // <- Arc Projectile Travel

        lifeTime += Time.deltaTime;

        if (HasProjectileLifeTimeExpire()) {
            DestroySelf();
        }
    }
}
