using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static event EventHandler OnEnemyDestroy;
    public static void ResetStaticData() {
        OnEnemyDestroy = null;
    }
    [SerializeField] private EnemyObjectSO enemyObjectSO;

    private HealthSystem healthSystem;

    private void Awake() {
        healthSystem = GetComponent<HealthSystem>();
    }

    private void Start() {
        healthSystem.OnDie += HealthSystem_OnDie;
        healthSystem.OnHit += HealthSystem_OnHit;
    }



    private void OnDestroy() {
        healthSystem.OnDie -= HealthSystem_OnDie;
        healthSystem.OnHit -= HealthSystem_OnHit;
    }

    private void HealthSystem_OnHit(object sender, EventArgs e) {
        //Spawn a on damage particle effect
        enemyObjectSO.SpawnParticleEffect(enemyObjectSO.hitParticlePrefab, transform.position);
    }

    private void HealthSystem_OnDie(object sender, System.EventArgs e) {
        enemyObjectSO.SpawnParticleEffect(enemyObjectSO.deathParticlePrefab, transform.position);
        DestroySelf();
    }

    private Transform SpawnDeathParticleEffect() {
        Vector3 offset = new Vector3(0, 1, 0);
        Transform particleEffect = Instantiate(enemyObjectSO.deathParticlePrefab, transform.position + offset, Quaternion.identity);
        return particleEffect;
    }



    public EnemyObjectSO GetEnemyObjectSO() {
        return enemyObjectSO;
    }

    public void DestroySelf() {
        OnEnemyDestroy?.Invoke(this, EventArgs.Empty);
        Destroy(gameObject);

    }

    public static Enemy SpawnEnemy(EnemyObjectSO enemyObjectSO, Vector3 spawnPosition) {
        Transform enemyTransform = Instantiate(enemyObjectSO.prefab, spawnPosition, Quaternion.identity);
        HealthSystem enemyHealthSystem = enemyTransform.GetComponent<HealthSystem>();
        Enemy enemy = enemyTransform.GetComponent<Enemy>();
        EnemyMovement enemyMovement= enemyTransform.GetComponent<EnemyMovement>();

        enemyTransform.position = spawnPosition;
        enemyHealthSystem.SetHealthMax(enemyObjectSO.healthMax);
        enemyMovement.SetMoveSpeed(enemyObjectSO.moveSpeed);
        enemyMovement.SetRotateSpeed(enemyObjectSO.rotateSpeed);
        return enemy;
    }


}
