using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CostUI : MonoBehaviour
{
    [SerializeField] ContainerNode containerNode;
    [SerializeField] TextMeshProUGUI text;

    private void Awake() {
        text.text = containerNode.GetCost().ToString();
    }



    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
