using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CurrencyUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI currencyText;
    [SerializeField] private Transform amountUpdatedTemplateUI;



    private void Start() {
        TowerGameManager.Instance.OnCurrencyChanged += TowerGameManager_OnCurrencyChanged;
    }

    private void OnDestroy() {
        TowerGameManager.Instance.OnCurrencyChanged -= TowerGameManager_OnCurrencyChanged;
    }

    private void TowerGameManager_OnCurrencyChanged(object sender, TowerGameManager.OnCurrencyChangedEventArgs e) {
        currencyText.text = "GOLD : " + e.currency.ToString();

        SpawnAmountUpdate(e.color, e.amount);
    }

    private AmountUpdatedTemplateUI SpawnAmountUpdate(Color color, int amount) {
        Transform amountUpdateTransform = Instantiate(amountUpdatedTemplateUI, transform);
        AmountUpdatedTemplateUI amountUpdateTemplate = amountUpdateTransform.GetComponent<AmountUpdatedTemplateUI>();
        amountUpdateTemplate.SetIsTemplate(false);
        amountUpdateTemplate.SetText(color, amount);

        return amountUpdateTemplate;
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
