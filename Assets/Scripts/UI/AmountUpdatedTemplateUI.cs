using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AmountUpdatedTemplateUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;

    private float timeVisibleMax = 0.5f;
    private float timeVisible;
    private float fadeSpeed = 10f;
    private bool isTemplate = true;
    private bool isInit = false;

    private void Start() {
        if (!isInit) {
            Hide();
        }
    }


    private void Update() {

        if (isTemplate) return;
        if (!isInit) return;

        timeVisible += Time.deltaTime;

        text.color = Color.Lerp(text.color, Color.clear, timeVisibleMax * fadeSpeed * Time.deltaTime);
        transform.position -= new Vector3(0, 1f, 0);

        if(timeVisible >= timeVisibleMax) {
            DestroySelf();
        }
    }

    private void DestroySelf() {
        Destroy(gameObject);
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }

    private void SetIsInit(bool value) {
        isInit = value;
    }

    public void SetIsTemplate(bool value) {
        isTemplate = value;
    }

    public void SetText(Color color, int amount) {

        if(color == Color.green) {
            text.color = color;
            text.text = "+" + amount.ToString();
        }

        if(color == Color.red) {
            text.color = color;
            text.text = "-" + amount.ToString();
        }

        SetIsInit(true);
        Show();

    }

}
