using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemyRemainingUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI enemiesRemainingText;

    private void Start() {
        TowerGameManager.Instance.OnEnemyCountChanged += TowerGameManager_OnEnemyCountChanged;

        Hide();
    }

    private void OnDestroy() {
        TowerGameManager.Instance.OnEnemyCountChanged -= TowerGameManager_OnEnemyCountChanged;
    }

    private void TowerGameManager_OnEnemyCountChanged(object sender, TowerGameManager.OnEnemyCountChangedEventArgs e) {
        if(e.enemyCountMax > 0) {
            Show();
        }
        else {
            enemiesRemainingText.text = "Enemies Remaining : ";
        }

        enemiesRemainingText.text = "Enemies Remaining : " + e.enemyCountMax.ToString();
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
