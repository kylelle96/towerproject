using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI waveCountSurvivedText;

    private void Start() {
        TowerGameManager.Instance.OnStateChanged += TowerGameManager_OnStateChanged;

        Hide();
    }

    private void OnDestroy() {
        TowerGameManager.Instance.OnStateChanged -= TowerGameManager_OnStateChanged;
    }

    private void TowerGameManager_OnStateChanged(object sender, System.EventArgs e) {
        if (TowerGameManager.Instance.IsGameOver()) {
            Show();
            waveCountSurvivedText.text = "YOU SURVIVED " + TowerGameManager.Instance.GetCurrentWave().ToString() + " WAVES"; 
        }
        else {
            Hide();
        }
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
