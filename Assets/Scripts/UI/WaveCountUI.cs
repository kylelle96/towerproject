using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveCountUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI waveCountText;

    private void Start() {
        TowerGameManager.Instance.OnWaveCountChanged += TowerGameManager_OnWaveCountChanged;

        Hide();
    }

    private void OnDestroy() {
        TowerGameManager.Instance.OnWaveCountChanged -= TowerGameManager_OnWaveCountChanged;
    }

    private void TowerGameManager_OnWaveCountChanged(object sender, TowerGameManager.OnWaveCountChangedEventArgs e) {
        waveCountText.text = "Wave : " + e.waveCount.ToString();
        Show();
    }


    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
