using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CountdownTimerUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI countdownTimerText;

    private void Start() {
        TowerGameManager.Instance.OnStateChanged += TowerGameManager_OnStateChanged;

        Hide();
    }



    private void OnDestroy() {
        TowerGameManager.Instance.OnStateChanged -= TowerGameManager_OnStateChanged;
    }

    private void TowerGameManager_OnStateChanged(object sender, System.EventArgs e) {
        if (TowerGameManager.Instance.IsCountdownToStartActive()) {
            Show();
        }
        else {
            Hide();
        }
    }


    private void Update() {
        countdownTimerText.text = Mathf.Ceil(TowerGameManager.Instance.GetCountdownToStartTimer()).ToString();
    }


    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
