using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveInitializeTimerUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI waveInitializeTimerText;

    private void Start() {
        TowerGameManager.Instance.OnStateChanged += TowerGameManager_OnStateChanged;

        Hide();
    }

    private void OnDestroy() {
        TowerGameManager.Instance.OnStateChanged -= TowerGameManager_OnStateChanged;
    }


    private void TowerGameManager_OnStateChanged(object sender, System.EventArgs e) {
        if (TowerGameManager.Instance.IsWaveInitializing()) {
            Show();
        }
        else {
            Hide();
        }
    }

    private void Update() {
        waveInitializeTimerText.text = Mathf.Ceil(TowerGameManager.Instance.GetWaveInitializeTimer()).ToString();
    }



    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }

}
