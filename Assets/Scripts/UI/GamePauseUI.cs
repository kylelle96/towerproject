using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePauseUI : MonoBehaviour
{
    [SerializeField] private Button mainMenuButton;
    [SerializeField] private Button resumeButton;

    private void Awake() {
        mainMenuButton.onClick.AddListener(() => {
            Loader.Load(Loader.Scene.MainMenuScene);
        });

        resumeButton.onClick.AddListener(() => {
            TowerGameManager.Instance.TogglePauseGame();
        });

    }
    private void Start() {
        TowerGameManager.Instance.OnGamePaused += TowerGameManager_OnGamePaused;
        TowerGameManager.Instance.OnGameUnpaused += TowerGameManager_OnGameUnpaused;

        Hide();
    }

    private void OnDestroy() {
        TowerGameManager.Instance.OnGamePaused -= TowerGameManager_OnGamePaused;
        TowerGameManager.Instance.OnGameUnpaused -= TowerGameManager_OnGameUnpaused;
    }

    private void TowerGameManager_OnGameUnpaused(object sender, System.EventArgs e) {
        Hide();
    }

    private void TowerGameManager_OnGamePaused(object sender, System.EventArgs e) {
        Show();
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
