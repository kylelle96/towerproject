using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public static event EventHandler<OnSetSpawnCountEventArgs> OnSetSpawnCount;
    public static void ResetStaticData() {
        OnSetSpawnCount = null;
    }
    public class OnSetSpawnCountEventArgs : EventArgs {
        public int spawnCount;
    }

    [SerializeField] private EnemyObjectSO enemyObjectSO;
    [SerializeField] private int spawnCountMax = 5;
    [SerializeField] private float spawnInBetweenTimerMax = 0.5f;

    private int currentSpawnCount = 0;
    private float spawnInBetweenTimer = 0;

    private void Start() {
        TowerGameManager.Instance.OnWaveInitialized += TowerGameManager_OnWaveInitialized;
    }

    private void OnDestroy() {
        TowerGameManager.Instance.OnWaveInitialized -= TowerGameManager_OnWaveInitialized;
    }

    private void TowerGameManager_OnWaveInitialized(object sender, TowerGameManager.OnWaveInitializedEventArgs e) {
        Debug.Log("Wave Initialize Complete!");
        SetSpawnCountMax(e.enemyCount);
        ResetSpawning();
        OnSetSpawnCount?.Invoke(this, new OnSetSpawnCountEventArgs {
            spawnCount = spawnCountMax
        }); ;
    }

    private void Update() {
        if (TowerGameManager.Instance.GetCurrentState() != TowerGameManager.State.GamePlaying) return;

        spawnInBetweenTimer += Time.deltaTime;

        if (spawnInBetweenTimer >= spawnInBetweenTimerMax) {

            spawnInBetweenTimer = 0;

            if (currentSpawnCount < spawnCountMax) {

                Enemy.SpawnEnemy(enemyObjectSO, this.transform.position + RandomOffset());
                currentSpawnCount++;
            }

        }

    }
    private void ResetCurrentSpawnCount() {
        this.currentSpawnCount = 0;
    }

    private Vector3 RandomOffset() {
        float x = UnityEngine.Random.Range(-5f, 5f);
        float z = UnityEngine.Random.Range(-5f, 5f);
        return new Vector3(x, 0, z);
    }

    public void SetSpawnCountMax(int spawnCountMax) {
        this.spawnCountMax = spawnCountMax;
    }

    private void ResetSpawning() {
        ResetCurrentSpawnCount();
    }

    

}
