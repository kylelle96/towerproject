using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    public event EventHandler OnHit;
    public event EventHandler<OnHealthChangedEventArgs> OnHealthChanged;
    public event EventHandler OnDie;

    public class OnHealthChangedEventArgs : EventArgs {
        public float healthNormalized;
    }

    [SerializeField] private float healthMax;
    [SerializeField] private float currentHealth;


    private void Awake() {
        currentHealth = healthMax;
    }

    public void InitializeHealth() {
        currentHealth = healthMax;
    }


    public void Damage(float damage) {
        currentHealth -= damage;

        if (currentHealth <= 0) {
            //Dead
            currentHealth = 0;
            OnDie?.Invoke(this, EventArgs.Empty);

        }
        OnHit?.Invoke(this, EventArgs.Empty);
        OnHealthChanged?.Invoke(this, new OnHealthChangedEventArgs() {
            healthNormalized = currentHealth / healthMax
        });
    }

    public float GetHealthMax() {
        return healthMax;
    }
    public void SetHealthMax(float healthMax) {
        this.healthMax = healthMax;
        InitializeHealth();
    }



}
