using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float rotateSpeed = 20f;

    private Vector3 targetMainBase;


    private void Start() {
        targetMainBase = MainBase.instance.transform.position + MainBase.instance.GetMainBaseTargetOffset();
    }

    private void Update() {
        if (targetMainBase != null) {
            Vector3 moveDir = (targetMainBase - transform.position).normalized;

            transform.position += moveDir * moveSpeed * Time.deltaTime;
            transform.forward = Vector3.Slerp(transform.forward, moveDir, rotateSpeed * Time.deltaTime);

        }
    }


    public void SetMoveSpeed(float moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    public void SetRotateSpeed(float rotateSpeed) {
        this.rotateSpeed = rotateSpeed;
    }
}
