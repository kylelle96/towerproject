using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Player : MonoBehaviour, ITowerObjectParent
{
    public event EventHandler<OnSelectedNodeChangedEventArgs> OnSelectedNodeChanged;
    public class OnSelectedNodeChangedEventArgs : EventArgs {
        public BaseNode selectedNode;
    }

    private const float MOVE_SPEED = 5f;

    public static Player Instance { get; private set; }

    [SerializeField] GameInput gameInput;
    [SerializeField] LayerMask nodeLayerMask;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float rotationSpeed = 20f;
    [SerializeField] private Transform capsuleTransform;
    [SerializeField] private Transform towerObjectPlacementPoint;

    private float dashSpeed = 20f;
    private float dashTimer;
    private float dashTimerMax = 0.1f;
    private bool isWalking = false;
    private Vector3 lastInteractionDirection;
    private BaseNode selectedNode;
    private TowerObject towerObject;
    public bool IsWalking { get { return isWalking; } }

    private void Awake() {
        if(Instance != null) {
            Debug.LogError("There are more than one player in the scene, Please remove dulplicated players!");
        }
        Instance = this;
    }

    private void Start() {
        gameInput.OnDashAction += GameInput_OnDashAction;
        gameInput.OnInteractAction += GameInput_OnInteractAction;
        gameInput.OnInteractAlternateAction += GameInput_OnInteractAlternateAction;
    }

    private void GameInput_OnInteractAlternateAction(object sender, EventArgs e) {

        if (!TowerGameManager.Instance.IsGamePlaying()) return;

        if (selectedNode != null) {
            selectedNode.InteractAlternate(this);
        }
    }

    private void GameInput_OnInteractAction(object sender, System.EventArgs e) {

        if (!TowerGameManager.Instance.IsGamePlaying()) return;

        if (selectedNode != null) {
            selectedNode.Interact(this);
        }
    }

    private void GameInput_OnDashAction(object sender, System.EventArgs e) {
        dashTimer = dashTimerMax;
    }

    private void Update() {

        HandleInteraction();
        HandleMovement();
        HandleDash();
    }

    private void HandleInteraction() {
        Vector2 inputVector = gameInput.GetMovementVectorNormalized();

        Vector3 moveDir = new Vector3(inputVector.x, 0f, inputVector.y);

        if(moveDir != Vector3.zero) {
            lastInteractionDirection = moveDir;
        }

        float interactDistance = 2f;
        RaycastHit raycastHit;
        if (Physics.Raycast(transform.position, lastInteractionDirection, out raycastHit, interactDistance, nodeLayerMask)){
            if(raycastHit.transform.TryGetComponent(out BaseNode baseNode)) {
                //Set SelectedClearNode
                if(baseNode != selectedNode) {
                    SetSelectedNode(baseNode);
                }
            }
            else {
                if(selectedNode != null) {
                    SetSelectedNode(null);
                }
            }
        }
        else {
            if(selectedNode != null) {
                SetSelectedNode(null);
            }
        }
    }


    private void HandleMovement() {
        Vector2 inputVector = gameInput.GetMovementVectorNormalized();

        Vector3 moveDir = new Vector3(inputVector.x, 0f, inputVector.y);

        float moveDistance = moveSpeed * Time.deltaTime;
        float playerRadius = 0.5f;
        float playerHeight = 2f;
        bool canMove = !Physics.CapsuleCast(transform.position, transform.position + Vector3.up * playerHeight, playerRadius, moveDir, moveDistance, nodeLayerMask);
        DebugHandler.DebugPlayerCapsule(transform, capsuleTransform, moveDir, moveDistance);

        if (!canMove) {
            //cannot move toward moveDir

            //Attempt only X Movement
            Vector3 moveDirX = new Vector3(moveDir.x, 0f, 0f).normalized;
            canMove = moveDir.x != 0 && !Physics.CapsuleCast(transform.position, transform.position + Vector3.up * playerHeight, playerRadius, moveDirX, moveDistance, nodeLayerMask);
            if (canMove) {
                //can only move on the X
                moveDir = moveDirX;
            }
            else {
                //cannot move only on the X

                //Attempt only move on the Z
                Vector3 moveDirZ = new Vector3(0f, 0f, moveDir.z).normalized;
                canMove = moveDir.z != 0 && !Physics.CapsuleCast(transform.position, transform.position + Vector3.up * playerHeight, playerRadius, moveDirZ, moveDistance);
                if (canMove) {
                    //can only move on the Z
                    moveDir = moveDirZ;
                }
                else {
                    //cannot move only on the Z
                }
            }

        }

        isWalking = moveDir != Vector3.zero;

        if (canMove) {
            transform.position += moveDir * moveSpeed * Time.deltaTime;
        }

        transform.forward = Vector3.Slerp(transform.forward, moveDir, rotationSpeed * Time.deltaTime);
    }

    private void HandleDash() {
        if (dashTimer > 0) {
            dashTimer -= Time.deltaTime;
            moveSpeed = dashSpeed;
        }
        else {
            moveSpeed = MOVE_SPEED;
        }
    }

    private void SetSelectedNode(BaseNode baseNode) {
        selectedNode = baseNode;
        OnSelectedNodeChanged?.Invoke(this, new OnSelectedNodeChangedEventArgs {
            selectedNode = selectedNode
        });
    }

    public void SetTeleportLocation(Vector3 teleportPosition) {
        transform.position = teleportPosition;
    }

    public Transform GetPlacementPoint() {
        return towerObjectPlacementPoint;
    }

    public void SetTowerObject(TowerObject towerObject) {
        this.towerObject = towerObject;
    }

    public TowerObject GetTowerObject() {
        return towerObject;
    }

    public bool HasTowerObject() {
        return towerObject != null;
    }

    public void ClearTowerObject() {
        towerObject = null;
    }
}

