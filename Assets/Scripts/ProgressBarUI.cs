using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarUI : MonoBehaviour
{
    [SerializeField] private MergerNode mergerNode;
    [SerializeField] private Image barImage;


    private void Start() {
        mergerNode.OnProgressChanged += MergerNode_OnProgressChanged;

        barImage.fillAmount = 0;

        Hide();
    }

    private void MergerNode_OnProgressChanged(object sender, MergerNode.OnProgressChangedEventArgs e) {
        barImage.fillAmount = e.progressNormalized;

        if(e.progressNormalized == 0f || e.progressNormalized == 1f) {
            Hide();
        }
        else {
            Show();
        }
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }
}
