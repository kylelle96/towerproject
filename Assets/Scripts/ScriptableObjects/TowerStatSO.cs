using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class TowerStatSO : ScriptableObject
{
    public TowerObjectSO towerObjectSO;
    public Transform projectilePrefab;
    public float damage;
    public float attackSpeed;
    public float range;

}
