using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class MergedRecipeSO : ScriptableObject
{
    public List<TowerObjectSO> inputList;
    public TowerObjectSO output;
    public int mergingProgressMax;
}
