using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ProjectileTypeSO : ScriptableObject
{
   public enum ProjectileType {
        Linear,
        Artillery,
        TripleShot,
        Railgun,
   }

    public ProjectileType projectileType;
    public float projectileSpeed;
    public float projectileDistance;
    public float projectileLifeTime;
}
