using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class EnemyObjectSO : ScriptableObject
{
    public string enemyName;
    public Transform prefab;
    public Transform deathParticlePrefab;
    public Transform hitParticlePrefab;
    public float damage = 10f;
    public float healthMax = 100f;
    public float moveSpeed = 10f;
    public float rotateSpeed = 20f;


    public Transform SpawnParticleEffect(Transform particlePrefab, Vector3 particlePosition) {
        Vector3 offset = new Vector3(0, 1, 0);
        Transform particleEffect = Instantiate(particlePrefab, particlePosition + offset, Quaternion.identity);
        return particleEffect;
    }
}
